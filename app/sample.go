package app

import (
	"fmt"
	"strconv"
	"strings"
)

// task 1
func Multiply(a, b float64) float64 {
	return a * b
}

// task 2
func IsInteger(value interface{}) (bool, error) {
	strValue := fmt.Sprintf("%v", value)
	_, err := strconv.Atoi(strValue)
	if err != nil {
		return false, fmt.Errorf("nilai %v bukanlah integer", value)
	}
	return true, nil
}

// task 3
func InToDayName(dayNumber int) string {
	days := []string{
		"senin",
		"selasa",
		"rabu",
		"kamis",
		"jumat",
		"sabtu",
		"minggu",
	}

	if dayNumber >= 1 && dayNumber <= 7 {
		return days[dayNumber-1]
	} else {
		return ""
	}
}

// task 4

var days = map[string]int{
	"senin":  1,
	"selasa": 2,
	"rabu":   3,
	"kamis":  4,
	"jumat":  5,
	"sabtu":  6,
	"minggu": 7,
}

func DayNameToInt(dayName string) int {

	dayNameLower := strings.ToLower(dayName)

	if number, found := days[dayNameLower]; found {
		return number
	}
	return 0
}

func IsHello(variable *string) {
	if *variable == "" {
		*variable = "hello world"
	}
}
