package main

import (
	"go-lth-1/app"
	"log"
)

func main() {
	// task 1
	log.Println(app.Multiply(7.8, 2))

	// task 2
	if isValid, err := app.IsInteger("0.1"); nil != err {
		log.Println(isValid, err.Error())
	} else {
		log.Println(isValid)
	}

	// task 3
	log.Println(app.InToDayName(3))

	// task 4
	log.Println(app.DayNameToInt("jumat"))

	var kosong string
	app.IsHello(&kosong)
	log.Println(kosong)
}
